;; -*- no-byte-compile: t; -*-
;;; .doom.d/packages.el

;;; Examples:
;; (package! some-package)
;; (package! another-package :recipe (:host github :repo "username/repo"))
;; (package! builtin-package :disable t)

;; (package! reverse-im)

(package! w3m)

(package! visual-fill-column)

(package! google-translate)

(package! ob-translate)

(package! emms)

(package! atomic-chrome)

(package! equake)

(package! lispy)

(package! evil-lispy)

(package! webkit-color-picker)

(package! writeroom-mode)

(package! hamlet-mode)

(package! gruber-darker-theme)

(package! highlight-symbol)

(package! exwm)

(package! tiny)

(package! ox-ipynb
  :recipe (:host github :repo "jkitchin/ox-ipynb"))

;; (package! elpy)

(package! company-tabnine)

;; (package! polymode)
;; (package! poly-org)
;; (package! poly-markdown)

(package! pulseaudio-control)

(package! emacs-udisksctl
  :recipe (:host github :repo "tosmi/emacs-udisksctl"))

(package! exwm-edit)

(package! bluetooth)

;; (package! subed
;;   :recipe (:host github :repo "rndusr/subed"))
