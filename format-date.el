;;; package --- Summary
;;; Format date before cursor with dots.

;;; Commentary:
;;; Because the dots are in the lower row and I have no numpad.

;;; Code:
(require 'dash)

(defvar default-date-separator ".")
(defvar separator-list '("." "/" "-" ":" "\\"))

(defun clever-format-date ()
  "Format date before cursor inserting dots or separator chosen \
when called with UNIVERSAL-ARGUMENT. Respects input method."
  (interactive)
  (let* ((word (progn
                 (backward-word)
                 (mark-word)
                 (buffer-substring-no-properties (mark) (point))))
         (word-as-list (map 'list
                            (lambda (char) (char-to-string char))
                            (string-to-list word)))
         (len (length word-as-list))
         (nums (map 'list
                    (lambda (num) (number-to-string num))
                    (number-sequence 0 9))))
    (if (and (-all? (lambda (val) (not (equal val nil)))
                    (map 'list
                         (lambda (str) (member str nums))
                         word-as-list))
             (or (equal len 6)
                 (equal len 8)))
        (progn
          (let ((sep (if (equal nil current-prefix-arg)
                         default-date-separator
                       (set-separator))))
            (delete-region (mark) (point))
            (insert (apply 'concat
                           (-insert-at 2 sep
                                       (-insert-at 4 sep word-as-list))))))
      (progn
        (deactivate-mark)
        (forward-word)
        (message "Ignoring: not a date.")))))

(defun set-separator ()
  "Set separator to use for date formatting."
  (let ((sep (char-to-string (read-char ">> " t))))
    (if (member sep separator-list)
        sep
      (progn
        (message "Not an allowed separator. Using default.")
        default-date-separator))))

;; 12122012
;; (global-set-key (kbd "C-\"") 'clever-format-date)

(provide 'format-date)
;;; format-date.el ends here
