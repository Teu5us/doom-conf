;;; ~/.doom.d/title.el -*- lexical-binding: t; -*-

(setq frame-title-format "%F: %b - %p")
