* My Doom Emacs Configuration
:PROPERTIES:
:header-args:emacs-lisp: :tangle config.el
:END:

#+html: <p align="center"><img src="./pic/transparency.png" width=500 /></p>

#+html: <p align="center"><img src="./pic/no-transparency.png" width=500 /></p>

** Doom
#+begin_src emacs-lisp
(setq +doom-dashboard-pwd-policy "~/")
(setq projectile-ignored-projects '("/tmp"))
(envrc-global-mode)
(setq lsp-haskell-process-path-hie "haskell-language-server-wrapper")
(setq rustic-lsp-server 'rust-analyzer)
;; (add-hook 'focus-out-hook #'garbage-collect)
(require 'tiny)
;; (add-hook 'prog-mode-hook 'highlight-symbol-mode)
(add-hook 'prog-mode-hook #'rainbow-delimiters-mode)

(setq large-file-warning-threshold nil)

(defvar banner-dirname (expand-file-name "~/.doom.d/banners/"))

(defvar banner-list
  (directory-files banner-dirname 'full (rx ".png" eos) 'sort)
  "A list of banners for Doom Dashboard.")

(defun set-random-banner ()
  (setq fancy-splash-image
        (nth (random (- (length banner-list) 1)) banner-list)))

(add-hook 'window-configuration-change-hook #'set-random-banner)

(load-file "~/.doom.d/format-date.el")

;; (defun format-date ()
;;   (interactive)
;;   (progn
;;     (backward-char 4)
;;     (insert ".")
;;     (backward-char 3)
;;     (insert ".")
;;     (forward-char 7)))

(defun kbf ()
  (interactive)
  (kill-buffer (buffer-name))
  (delete-frame))

(global-set-key (kbd "C-x 9") 'kbf)

;; for marker navigation
(defun go-to-marker ()
  (interactive)
  (search-forward "<++>")
  (delete-backward-char 4)
  (evil-insert 0))

(defun insert-marker ()
  (interactive)
  (insert "<++>"))

(set-default 'tab-always-indent 'complete)

;; Set transparency of emacs
(defun transparency (value)
  "Sets the transparency of the frame window. 0=transparent/100=opaque"
  (interactive "nTransparency Value 0 - 100 opaque: ")
  (set-frame-parameter (selected-frame) 'alpha value))

(defun my/scroll-other-window ()
  (interactive)
  (let* ((wind (other-window-for-scrolling))
         (mode (with-selected-window wind major-mode)))
    (if (eq mode 'pdf-view-mode)
        (with-selected-window wind
          (progn
            (pdf-view-next-line-or-next-page 2)
            (other-window 1)))
      (scroll-other-window 2))))

(defun my/scroll-other-window-down ()
  (interactive)
  (let* ((wind (other-window-for-scrolling))
         (mode (with-selected-window wind major-mode)))
    (if (eq mode 'pdf-view-mode)
        (with-selected-window wind
          (progn
            (pdf-view-previous-line-or-previous-page 2)
            (other-window 1)))
      (scroll-other-window-down 2))))

(global-set-key (kbd "C-M-v") 'my/scroll-other-window)
(global-set-key (kbd "C-M-S-v") 'my/scroll-other-window-down)

;; (defun my-end-of-buffer ()
;;   "Go to beginning of last line in buffer.
;; If last line is empty, go to beginning of penultimate one
;; instead."
;;   (interactive)
;;   (goto-char (point-max))
;;   (beginning-of-line (and (looking-at-p "^$") 0)))

;; (global-set-key [remap erd-of-buffer] #'my-end-of-buffer)

(add-hook 'proced-mode-hook 'evil-emacs-state)
#+end_src

** Garbage collection
*** v1
#+BEGIN_SRC emacs-lisp
;; (defvar file-name-handler-alist-backup
;;         file-name-handler-alist)
;; (setq gc-cons-threshold most-positive-fixnum
;;       file-name-handler-alist nil)
;; (add-hook 'after-init-hook
;;   (lambda ()
;;     (garbage-collect)
;;     (setq gc-cons-threshold
;;             (car (get 'gc-cons-threshold 'standard-value))
;;       file-name-handler-alist
;;         (append
;;           file-name-handler-alist-backup
;;           file-name-handler-alist))))

;; (setq gnutls-algorithm-priority "NORMAL:-VERS-TLS1.3")
#+END_SRC
*** v2
#+BEGIN_SRC emacs-lisp
(defvar better-gc-cons-threshold 67108864 ; 64mb
  "The default value to use for `gc-cons-threshold'.

If you experience freezing, decrease this.  If you experience stuttering, increase this.")

(add-hook 'emacs-startup-hook
          (lambda ()
            (setq gc-cons-threshold better-gc-cons-threshold)
            ;; (setq file-name-handler-alist file-name-handler-alist-original)
            ;; (makunbound 'file-name-handler-alist-original)
            ))

(add-hook 'emacs-startup-hook
          (lambda ()
            (if (boundp 'after-focus-change-function)
                (add-function :after after-focus-change-function
                              (lambda ()
                                (unless (frame-focus-state)
                                  (garbage-collect))))
              (add-hook 'after-focus-change-function 'garbage-collect))
            (defun gc-minibuffer-setup-hook ()
              (setq gc-cons-threshold (* better-gc-cons-threshold 2)))

            (defun gc-minibuffer-exit-hook ()
              (garbage-collect)
              (setq gc-cons-threshold better-gc-cons-threshold))

            (add-hook 'minibuffer-setup-hook #'gc-minibuffer-setup-hook)
            (add-hook 'minibuffer-exit-hook #'gc-minibuffer-exit-hook)))
#+END_SRC
** Speed this thing up
#+BEGIN_SRC emacs-lisp
(setq doom-modeline-enable-word-count nil)
#+END_SRC

** Eshell
*** Eshell & Equake
#+begin_src emacs-lisp
(set-eshell-alias!
 "doom" "{~/.emacs.d/bin/doom $1}"
 "dc" "find-file-other-frame '~/.doom.d/README.org'"
 "hep" "find-file-other-frame '~/.config/nixpkgs/config.nix'"
 "hec" "find-file-other-frame '~/.config/nixpkgs/home.nix'"
 "eec" "find-file-other-frame '~/.config/nixpkgs/derive/emacs/default.nix'"
 "ff" "find-file-other-frame $1"
 "r" "dired-other-frame $1"
 "mkd" "{mkdir -p $*}"
 "nix" "PAGER='' nix $*"
 "nix-env-s" "{PAGER='' nix-env -f '<nixpkgs>' $*}"
 "nix-env-u" "{PAGER='' nix-env -f '<unstable>' $*}"
 "run" "{nohup $* 2>&1 >/dev/null &}")
(require 'em-smart)
(setq eshell-where-to-jump 'begin)
(setq eshell-review-quick-commands nil)
(setq eshell-smart-space-goes-to-end t)
(add-hook 'eshell-mode-hook 'eshell-smart-initialize)
(require 'esh-module)
(add-to-list 'eshell-modules-list 'eshell-tramp)

;; equake
(require 'equake)
(setq equake-size-width 0.6)
(setq equake-size-height 0.4)
(setq equake-opacity-active 0.9)
(setq equake-opacity-inactive 0.9)
(global-set-key (kbd "C-x C-c") 'equake-check-if-in-equake-frame-before-closing)
(setq equake-default-shell 'vterm)
(defun equake-text-scale ()
  (text-scale-increase 0)
  (evil-collection-vterm-toggle-send-escape))
(add-hook 'equake-mode-hook #'equake-text-scale)

(add-hook 'eshell-mode-hook
          (lambda ()
            (define-key eshell-mode-map (kbd "C-l")
              (lambda () (interactive) (eshell/clear 1) (eshell-emit-prompt)))))

(add-hook! 'eshell-mode-hook
  (defun +eshell-init-company-h ()
    (company-mode +1)
    (setq-local company-idle-delay nil)
    (setq-local company-backends '(company-pcomplete))
    (setq-local company-frontends
                (cons 'company-tng-frontend company-frontends))
    (when (bound-and-true-p evil-local-mode)
      (evil-normalize-keymaps))))

(add-hook 'eshell-mode-hook
          (lambda ()
            (map! :map eshell-mode-map
                  "TAB" #'+eshell/pcomplete
                  [tab] #'+eshell/pcomplete)))


#+end_src

** Scheme/Lisp/Hy
#+BEGIN_SRC emacs-lisp
(require 'lispy)
(require 'evil-lispy)

(defun conditionally-enable-lispy ()
  (when (or (eq this-command 'eval-expression)
            (eq this-command 'pp-eval-expression))
    (lispy-mode 1)))
(add-hook 'minibuffer-setup-hook 'conditionally-enable-lispy)

(add-hook 'scheme-mode-hook #'evil-lispy-mode)
(add-hook 'lisp-mode-hook #'evil-lispy-mode)
(add-hook 'emacs-lisp-mode-hook #'evil-lispy-mode)
(add-hook 'hy-mode-hook #'evil-lispy-mode)

(defun set-scheme ()
  (make-variable-buffer-local (defvar scheme nil))
    (add-hook 'window-configuration-change-hook
              (lambda () (setq geiser-default-implementation scheme)) nil t))

(add-hook 'text-mode-hook #'set-scheme)
(add-hook 'prog-mode-hook #'set-scheme)

(add-hook 'hy-mode-hook
          #'(lambda ()
              (set (make-local-variable 'hy-shell--interpreter)
                   (executable-find "hy"))))

(map! :map hy-mode-map
      "C-c c" #'run-jedhy)

(defun ni/geiser-set-scheme (impl)
  "Associates current buffer with a given Scheme implementation."
  (interactive)
  (save-excursion
    (geiser-syntax--remove-kws)
    (geiser-impl--set-buffer-implementation impl)
    (geiser-repl--set-up-repl impl)
    (geiser-syntax--add-kws)
    (geiser-syntax--fontify)))

(eval-after-load 'sly
  (setq inferior-lisp-program (executable-find "quicklisp-lisp-launcher.sh")))
#+END_SRC
** Python
#+BEGIN_SRC emacs-lisp
(require 'ox-ipynb)
(require 'elpy)
(setq elpy-rpc-virtualenv-path 'global)
(elpy-enable)

(map! :map python-mode-map
      :nvi "M-." 'elpy-goto-definition
      :nvi "M-," 'pop-tag-mark)

(setq lsp-python-ms-executable (executable-find "python-language-server"))
(after! lsp-python-ms
  (set-lsp-priority! 'mspyls 1))

(defun py-completion-setup ()
  (set (make-local-variable 'company-backends)
       '(elpy-company-backend
         company-capf
         company-yasnippet)))

(add-hook 'ein:notebook-mode-hook #'py-completion-setup)

(add-to-list 'org-structure-template-alist '("j" . "src jupyter-python :async yes"))
#+END_SRC
** Helm
#+begin_src emacs-lisp
;; (require 'helm-external)
;; (setq helm-autoresize-mode t)
;; (setq helm-ff-auto-update-initial-value t)

;; (defun make-assoc (name list assoc)
;;   (setq name
;;         (pairlis list
;;                  (make-list
;;                   (length list)
;;                   `assoc))))

;; (setq vim-fts '("md" "rs" "vim" "sh" "elm"))
;; (setq vim-assoc (pairlis vim-fts (make-list (length vim-fts) '"gnvim")))

;; (setq helm-external-programs-associations
;;       (append vim-assoc
;;               '(("odt" . "soffice")
;;                 ("ods" . "soffice")
;;                 ("odp" . "soffice")
;;                 ("pdf" . "zathura")
;;                 ("djvu" . "zathura")
;;                 ("html" . "chromium"))))

;; (defun helm-open-loop (list)
;;   (while (< 0 (length list))
;;     (helm-open-file-externally (car list))
;;     (helm-open-loop (cdr list))))

;; (defun ranger-helm-open-externally (&optional list)
;;   (interactive)
;;   (unless list (setq list (delete-dups (dired-get-marked-files))))
;;   (helm-open-loop list))

;; (defun eshell/open (&rest files)
;;   (helm-open-loop files))

;; (add-hook 'emacs-startup-hook
;;   (lambda () (define-key ranger-mode-map (kbd "wo") 'ranger-helm-open-externally)))
#+end_src                                           
** Tune scratch buffer
#+BEGIN_SRC emacs-lisp
(setq doom-scratch-buffer-major-mode 'org-mode)
#+END_SRC
** Keybindings
*** General
#+BEGIN_SRC emacs-lisp
(map!
 ;; ^/$
 (:leader
  :n "H" 'evil-first-non-blank)
 (:leader
  :n "L" 'evil-end-of-line)
 ;; Switch buffers
 (:n "gt" 'next-buffer)
 (:n "gT" 'previous-buffer)
 ;; Inc/Dec
 (:nv "g-" 'evil-numbers/dec-at-pt)
 (:nv "g+" 'evil-numbers/inc-at-pt)
 (:leader
  :n "tC" 'rainbow-mode)
 (:leader
  :n "om" 'mu4e)
 (:leader
  :n "qk" 'kbf)
 (:leader
  :n "bD" 'kill-buffer-and-window)
 (:leader
  :n "tz" 'writeroom-mode))

(global-set-key (kbd "M-<tab>") 'completion-at-point)
(global-set-key (kbd "C->") 'tiny-expand)
(global-set-key (kbd "C-s") 'counsel-grep-or-swiper)

(global-set-key (kbd "M-1") 'winum-select-window-1)
(global-set-key (kbd "M-2") 'winum-select-window-2)
(global-set-key (kbd "M-3") 'winum-select-window-3)
(global-set-key (kbd "M-4") 'winum-select-window-4)
(global-set-key (kbd "M-5") 'winum-select-window-5)
(global-set-key (kbd "M-6") 'winum-select-window-6)
(global-set-key (kbd "M-7") 'winum-select-window-7)
(global-set-key (kbd "M-8") 'winum-select-window-8)
(global-set-key (kbd "M-9") 'winum-select-window-9)

(define-key evil-insert-state-map (kbd "C-\"") 'clever-format-date)
(define-key evil-normal-state-map (kbd "M-c") 'go-to-marker)
(define-key evil-insert-state-map (kbd "M-c") 'go-to-marker)
(define-key evil-normal-state-map (kbd "C-<") 'insert-marker)
(define-key evil-insert-state-map (kbd "C-<") 'insert-marker)
(defun extra-keys ()
  (keyboard-translate ?\C-t ?\C-x)
  (keyboard-translate ?\C-x ?\C-t))
(add-hook '+doom-dashboard-mode-hook #'extra-keys)
#+END_SRC

*** Markdown
#+BEGIN_SRC emacs-lisp
(map!
 :map markdown-mode-map
 :localleader (
 ;; Syntax
               :nv "tb" 'markdown-insert-bold
               :nv "tb" 'markdown-insert-gfm-checkbox
               :nv "ti" 'markdown-insert-italic
               :nv "tc" 'markdown-insert-code
               :nv "tC" 'markdown-insert-gfm-code-block
               :nv "tL" 'markdown-insert-link
               :nv "tt" 'markdown-insert-table
               :n "tl" 'markdown-insert-list-item
 ;; Headers
               :n "h1" 'markdown-insert-header-atx-1
               :n "h2" 'markdown-insert-header-atx-2
               :n "h3" 'markdown-insert-header-atx-3
               :n "h4" 'markdown-insert-header-atx-4))
#+END_SRC
*** EMMS
#+BEGIN_SRC emacs-lisp
(map!
 :leader (
         :nv "ep" 'emms-pause
         :nv "es" 'emms-stop
         :nv "eo" 'emms-play-file
         :nv "ef" 'emms-seek-forward
         :nv "eb" 'emms-seek-backward
         :nv "e=" 'emms-volume-raise
         :nv "e-" 'emms-volume-lower))
#+END_SRC                                                                                              
*** term
#+BEGIN_SRC emacs-lisp
(map!
 :map vterm-mode-map (
                      :ni "C-j" 'vterm-send-down
                      :ni "C-k" 'vterm-send-up))
#+END_SRC

*** w3m
#+BEGIN_SRC emacs-lisp
(map!
 :map w3m-mode-map (:n "M-l" 'w3m-lnum-follow))
#+END_SRC
** Spelling settings
Uses hunspell.
#+BEGIN_SRC emacs-lisp
(with-eval-after-load 'ispell
  (progn
    (defun ispell-get-coding-system () 'utf-8)
    (setenv "LANG" "en_US.UTF-8")
    (setq ispell-program-name (executable-find "hunspell")
            ispell-dictionary "ru_RU,en_GB,en_US"
            ispell-personal-dictionary "~/.doom.d/personal_dict")
    (ispell-set-spellchecker-params)
    (ispell-hunspell-add-multi-dic ispell-dictionary)
    (unless (file-exists-p ispell-personal-dictionary)
        (write-region "" nil ispell-personal-dictionary nil 0))))
#+END_SRC
** Abbrevs
#+BEGIN_SRC emacs-lisp
(setq abbrev-file-name "~/.doom.d/abbrevs.el")
(let ((md-abbr "~/.doom.d/md-abbr.el"))
  (when (file-exists-p md-abbr)
    (load-file md-abbr)))
#+END_SRC
** Org
#+BEGIN_SRC emacs-lisp
(require 'org)
(setq org-todo-keywords '((sequence "T" "0" "|" "V")
                          (sequence "&" "|" "X")))
(add-to-list 'org-latex-packages-alist
             '("AUTO" "babel" t ("pdflatex")))
(add-to-list 'org-latex-packages-alist
             '("AUTO" "polyglossia" t ("xelatex" "lualatex")))
;; Then use '#+LANGUAGE: ru' to get Russian

(setq org-default-notes-file (concat org-directory "/notes.org"))
(setq-default org-display-custom-times t)
(setq org-time-stamp-custom-formats '("<%d.%m.%Y>" . "<%d.%m.%Y %H:%M>"))

(defun my-org-start-hook ()
  (turn-off-auto-fill)
  ;; (visual-fill-column-mode)
  (+word-wrap-mode))

(add-hook 'org-mode-hook #'my-org-start-hook)

(defvar haskell-process-args-ghci '("-ignore-dot-ghci" "-ferror-spans"))

(defun org-global-props (&optional property buffer)
  "Get the plists of global org properties of current buffer."
  (unless property (setq property "PROPERTY"))
  (with-current-buffer (or buffer (current-buffer))
    (org-element-map (org-element-parse-buffer) 'keyword (lambda (el) (when (string-match property (org-element-property :key el)) el)))))

(defun org-global-prop-value (key)
  "Get global org property KEY of current buffer."
  (org-element-property :value (car (org-global-props key))))

(add-hook 'org-mode-hook
          #'(lambda () (make-local-variable 'org-log-note-headings)))

(advice-add #'org-store-log-note :before
            #'(lambda (&rest r)
                (if (equal org-lang "ru")
                    (setcdr (assq 'note org-log-note-headings) '(. "Заметка от %t"))
                  (setcdr (assq 'note org-log-note-headings) '(. "Note taken on %t")))))

(defun my-org-add-log-setup (&optional purpose state prev-state how lang extra)
  "LANG is passed automatically based on `#+LANGUAGE' value
for other parameters see original `org-add-log-setup'"
  (move-marker org-log-note-marker (point))
  (setq org-log-note-purpose purpose
        org-log-note-state state
        org-log-note-previous-state prev-state
        org-log-note-how how
        org-lang lang
        org-log-note-extra extra
        org-log-note-effective-time (org-current-effective-time))
  (add-hook 'post-command-hook 'org-add-log-note 'append))

(advice-add #'org-add-note :override
            #'(lambda (&rest r)
                (interactive)
                (my-org-add-log-setup 'note nil nil nil (org-global-prop-value "language") nil)))

;; (map!
;;  :map org-mode-map
;;  :nv "j" 'evil-next-visual-line
;;  :nv "k" 'evil-previous-visual-line)
#+END_SRC
** Insert date from calendar
#+BEGIN_SRC emacs-lisp
(defun calendar-insert-date ()
  "Capture the date at point, exit the Calendar, insert the date."
  (interactive)
  (seq-let (month day year) (save-match-data (calendar-cursor-to-date))
    (calendar-exit)
    (insert (format "%02d.%02d.%d" day month year))))

(define-key calendar-mode-map (kbd "RET") 'calendar-insert-date)
#+END_SRC
** Google Translate
#+BEGIN_SRC emacs-lisp
(require 'google-translate)
(require 'google-translate-smooth-ui)
(setq google-translate-backend-method 'curl)
(map!
 :map markdown-mode-map
 :leader (
          :nv "C-t" 'google-translate-smooth-translate)
 )
#+END_SRC
** Function to run compiler script
#+BEGIN_SRC emacs-lisp
(defun compiler ()
  (interactive)
  (save-window-excursion
    (save-buffer)
    (async-shell-command
     (format "compiler %s"
             (shell-quote-argument
              (buffer-file-name))))))
#+END_SRC
** Deft
#+BEGIN_SRC emacs-lisp
(setq deft-directory "~/org")
#+END_SRC
** Markdown
#+BEGIN_SRC emacs-lisp
(add-to-list 'auto-mode-alist '("\\.rmd\\'" . markdown-mode))
(setq markdown-hide-urls t)
#+END_SRC
** Enable images in w3m
#+BEGIN_SRC emacs-lisp
(setq w3m-default-display-inline-images t)
#+END_SRC
** EMMS
#+BEGIN_SRC emacs-lisp
(require 'emms-setup)
(emms-all)
(emms-default-players)
#+END_SRC
** Atomic-Chrome
#+BEGIN_SRC emacs-lisp
(setq atomic-chrome-default-major-mode 'markdown-mode)
(setq atomic-chrome-buffer-open-style 'frame)
(atomic-chrome-start-server)
#+END_SRC
** Smooth scrolling
#+BEGIN_SRC emacs-lisp
;; Vertical Scroll
(setq scroll-step 1)
(setq scroll-margin 1)
(setq scroll-conservatively 101)
(setq scroll-up-aggressively 0.01)
(setq scroll-down-aggressively 0.01)
(setq auto-window-vscroll nil)
(setq fast-but-imprecise-scrolling nil)
(setq mouse-wheel-scroll-amount '(1 ((shift) . 1)))
(setq mouse-wheel-progressive-speed nil)
;; Horizontal Scroll
(setq hscroll-step 1)
(setq hscroll-margin 1)
#+END_SRC

** Set font and theme
#+BEGIN_SRC emacs-lisp
(setq doom-font (font-spec :family "monospace" :size 20))
(setq doom-theme 'doom-molokai)
(load-file "~/.doom.d/small-look.el")
#+END_SRC
** Relative line numbers
#+BEGIN_SRC emacs-lisp
(setq display-line-numbers-type 'relative)
#+END_SRC
** Russian support
#+BEGIN_SRC emacs-lisp
(load-file "~/.doom.d/ru-dvorak.el")
(require 'ru-dvorak)
(set-input-method 'ru-dvorak)
(toggle-input-method)
#+END_SRC
*** Support input method for evil by Khaoos
Taken from [[https://github.com/emacs-evil/evil/issues/605][this issue]] and [[https://github.com/khaoos-abominable/dotfiles/blob/master/spacemacs/dotspacemacs][this dotspacemacs]]
#+BEGIN_SRC emacs-lisp
;; Rebind commands that don't respect input method
(map!
 :n "r" 'khaoos-evil-replace
 :leader (:nvo "sc" 'evil-khaoos-avy-goto-char
               :nvo "sC" 'evil-khaoos-avy-goto-char-2
               :nvo "sw" 'evil-khaoos-avy-goto-word-or-subword-1))

(evil-define-key 'normal evil-snipe-local-mode-map "f" 'khaoos-evil-find-char)
(evil-define-key 'normal evil-snipe-local-mode-map "F" 'khaoos-evil-find-char-backward)
(evil-define-key 'normal evil-snipe-local-mode-map "t" 'khaoos-evil-find-char-to)
(evil-define-key 'normal evil-snipe-local-mode-map "T" 'khaoos-evil-find-char-to-backward)
(evil-define-key 'normal evil-snipe-local-mode-map "s" 'khaoos-evil-snipe-s)
(evil-define-key 'normal evil-snipe-local-mode-map "S" 'khaoos-evil-snipe-S)

(evil-define-key 'visual evil-snipe-local-mode-map "f" 'khaoos-evil-find-char)
(evil-define-key 'visual evil-snipe-local-mode-map "F" 'khaoos-evil-find-char-backward)
(evil-define-key 'visual evil-snipe-local-mode-map "t" 'khaoos-evil-find-char-to)
(evil-define-key 'visual evil-snipe-local-mode-map "T" 'khaoos-evil-find-char-to-backward)
(evil-define-key 'visual evil-snipe-local-mode-map "z" 'khaoos-evil-snipe-s)
(evil-define-key 'visual evil-snipe-local-mode-map "Z" 'khaoos-evil-snipe-S)

(evil-define-key 'operator evil-snipe-local-mode-map "f" 'khaoos-evil-find-char)
(evil-define-key 'operator evil-snipe-local-mode-map "F" 'khaoos-evil-find-char-backward)
(evil-define-key 'operator evil-snipe-local-mode-map "t" 'khaoos-evil-find-char-to)
(evil-define-key 'operator evil-snipe-local-mode-map "T" 'khaoos-evil-find-char-to-backward)
(evil-define-key 'operator evil-snipe-local-mode-map "z" 'khaoos-evil-snipe-s)
(evil-define-key 'operator evil-snipe-local-mode-map "Z" 'khaoos-evil-snipe-S)

(defvar khaoos-input-method-last-raw-key nil
  "The last key pressed with an input method switched on but ignoring conversion
of the input method.")

(defun khaoos-capture-input-mode-raw-key (key)
  "Function captures an input key ignoring the current input method.
Doesn't work for complex input methods which use event loops."
  (setq khaoos-input-method-last-raw-key (char-to-string key)))

(defun khaoos-activate-input-method (input-method)
  "Defines an advise for a function which implements current input method."
  ;; We don't bother ourselves to remove the advise when we deactivate the input method.
  ;; The chances are high that we'll reuse it.
  ;; We may end up with several advices for different input methods if an user uses them.
  ;; It doesn't matter as the only one work at the moment.
  ;; I saw a case when input-method-function was equal to 'list'! So there is addition check
  ;; on current-input-method
  (if (and current-input-method input-method-function)
      (advice-add input-method-function :before #'khaoos-capture-input-mode-raw-key)))

(advice-add 'activate-input-method :after #'khaoos-activate-input-method)

(defcustom khaoos-evil-escape-ignore-input-method nil
  "If non-nil then the key sequence can be entered ignoring the current input method if any."
  :type 'boolean
  :group 'evil-escape)

(defun khaoos-evil-escape-p ()
  "Return non-nil if evil-escape can run.
Edited by khaoos to implement the ability of ignoring the input method"
  (and evil-escape-key-sequence
       (not evil-escape-inhibit)
       (or (window-minibuffer-p)
           (bound-and-true-p isearch-mode)
           (memq major-mode '(ibuffer-mode
                              image-mode))
           (evil-escape--is-magit-buffer)
           (and (fboundp 'helm-alive-p) (helm-alive-p))
           (or (not (eq 'normal evil-state))
               (not (eq 'evil-force-normal-state
                        (lookup-key evil-normal-state-map [escape])))))
       (not (memq major-mode evil-escape-excluded-major-modes))
       (not (memq evil-state evil-escape-excluded-states))
       (or (not evil-escape-enable-only-for-major-modes)
           (memq major-mode evil-escape-enable-only-for-major-modes))
       (or (equal (this-command-keys) (evil-escape--first-key))
           (and khaoos-evil-escape-ignore-input-method ;;khaoos+
                current-input-method ;;khaoos+
                (equal khaoos-input-method-last-raw-key (evil-escape--first-key))) ;;khaoos+
           (and evil-escape-unordered-key-sequence
                (or (equal (this-command-keys) (evil-escape--second-key))))
           (and evil-escape-unordered-key-sequence ;;khaoos+
                khaoos-evil-escape-ignore-input-method ;;khaoos+
                current-input-method ;;khaoos+
                (equal khaoos-input-method-last-raw-key (evil-escape--second-key)))) ;;khaoos+
       (not (cl-reduce (lambda (x y) (or x y))
                       (mapcar 'funcall evil-escape-inhibit-functions)
                       :initial-value nil))))

(defun khaoos-evil-escape-pre-command-hook ()
  "evil-escape pre-command hook.
Edited by khaoos to implement the ability of ignoring the input method"
  (with-demoted-errors "evil-escape: Error %S"
      (when (khaoos-evil-escape-p)
        (let* ((modified (buffer-modified-p))
               (inserted (evil-escape--insert))
               (fkey (elt evil-escape-key-sequence 0))
               (skey (elt evil-escape-key-sequence 1))
               (evt (read-event nil nil evil-escape-delay)))
          (when inserted (evil-escape--delete))
          (set-buffer-modified-p modified)
          (cond
           ((and (characterp evt)
                 (or (and (or (equal (this-command-keys) (evil-escape--first-key)) ;;khaoos*
                              (and khaoos-evil-escape-ignore-input-method ;;khaoos+
                                   current-input-method ;;khaoos+
                                   (equal khaoos-input-method-last-raw-key (evil-escape--first-key)))) ;;khaoos+
                          (char-equal evt skey))
                     (and evil-escape-unordered-key-sequence
                          (or (equal (this-command-keys) (evil-escape--second-key)) ;;khaoos*
                              (and khaoos-evil-escape-ignore-input-method ;;khaoos+
                                   current-input-method ;;khaoos+
                                   (equal khaoos-input-method-last-raw-key (evil-escape--second-key)))) ;;khaoos+
                          (char-equal evt fkey))))
            (evil-repeat-stop)
            (when (evil-escape-func) (setq this-command (evil-escape-func))))
           ((null evt))
           (t (setq unread-command-events
                    (append unread-command-events (list evt)))))))))

(advice-add 'evil-escape-pre-command-hook :override #'khaoos-evil-escape-pre-command-hook)

(defun khaoos-evil-read-key-respect-input-method (evil-read-key-result)
  "Gets the result of evil-read-key function and converts it according the current input method
which at the moment could be a method of a family of quail input methods"
  (if (and (characterp evil-read-key-result)
           current-input-method
           (equal input-method-function 'quail-input-method))
    (let* ((translated-key-list (quail-lookup-key (char-to-string evil-read-key-result)))
           (translated-key (if (equal (length translated-key-list) 1)
                               (car translated-key-list)
                               evil-read-key-result)))
          translated-key)
    evil-read-key-result))

(advice-add 'evil-read-key :filter-return 'khaoos-evil-read-key-respect-input-method)

(defun khaoos-run-evil-command-respect-input-method (evil-command)
  "Runs interactively evil command evil-command which now respects the current input method"
  ;; if we are in the mode which prohibits input method we do a trick
  (if (and evil-input-method (not current-input-method))
      (evil-without-input-method-hooks
        (activate-input-method evil-input-method)
        (condition-case err
            (call-interactively evil-command)
          (error
            (inactivate-input-method)
            (signal (car err) (cdr err))))
        (inactivate-input-method))
    (call-interactively evil-command)))

(with-eval-after-load 'evil-macros
  (evil-define-operator khaoos-evil-replace ()
    "Wrapper of evil-replace to make it respect input method"
    (interactive)
    (khaoos-run-evil-command-respect-input-method 'evil-replace))

  (evil-define-motion khaoos-evil-find-char ()
    "Wrapper of evil-find-char to make it respect input method"
    :type exclusive
    (interactive)
    (khaoos-run-evil-command-respect-input-method 'evil-snipe-f))

  (evil-define-motion khaoos-evil-find-char-to ()
    "Wrapper of evil-find-char-to to make it respect input method"
    :type exclusive
    (interactive)
    (khaoos-run-evil-command-respect-input-method 'evil-snipe-t))

  (evil-define-motion khaoos-evil-find-char-backward ()
    "Wrapper of evil-find-char-backward to make it respect input method"
    :type exclusive
    (interactive)
    (khaoos-run-evil-command-respect-input-method 'evil-snipe-F))

  (evil-define-motion khaoos-evil-find-char-to-backward ()
    :type exclusive
    (interactive)
    (khaoos-run-evil-command-respect-input-method 'evil-snipe-T))

  (evil-define-motion khaoos-evil-snipe-s ()
    (interactive)
    (khaoos-run-evil-command-respect-input-method 'evil-snipe-s))

  (evil-define-motion khaoos-evil-snipe-S ()
    (interactive)
    (khaoos-run-evil-command-respect-input-method 'evil-snipe-S))

  (evil-define-motion khaoos-evil-snipe-x ()
    (interactive)
    (khaoos-run-evil-command-respect-input-method 'evil-snipe-x))

  (evil-define-motion khaoos-evil-snipe-X ()
    (interactive)
    (khaoos-run-evil-command-respect-input-method 'evil-snipe-X))

  (evil-define-operator khaoos--insert-one-char ()
    "Switches to insert mode just to input one character"
    (interactive)
    (let ((a (read-char "Input a character to insert:" t)))
      (insert-char a)))

  (evil-define-operator khaoos-insert-one-char ()
    "Switches to insert mode just to input one character"
    (interactive)
    (khaoos-run-evil-command-respect-input-method 'khaoos--insert-one-char))

  (evil-define-operator khaoos--append-one-char ()
    "Switches to insert mode just to append one character"
    (interactive)
    (let ((a (read-char "Input a character to append:" t)))
      (unless (eolp) (forward-char))
      (insert-char a)
		  (unless (eolp) (backward-char))))

  (evil-define-operator khaoos-append-one-char ()
    "Switches to insert mode just to input one character"
    (interactive)
    (khaoos-run-evil-command-respect-input-method 'khaoos--append-one-char))
)


(with-eval-after-load 'evil-integration
  (defun khaoos-avy-goto-char ()
    "Make `evil-avy-go-to-char' respect the current input method"
    (interactive)
    (khaoos-run-evil-command-respect-input-method 'avy-goto-char))

  (evil-define-avy-motion khaoos-avy-goto-char inclusive)

  (defun khaoos-avy-goto-char-2 ()
    "Make `evil-avy-go-to-char-2' respect the current input method"
    (interactive)
    (khaoos-run-evil-command-respect-input-method 'avy-goto-char-2))

  (evil-define-avy-motion khaoos-avy-goto-char-2 inclusive)

  (defun khaoos-avy-goto-word-or-subword-1 ()
    "Make `evil-avy-go-to-char' respect the current input method"
    (interactive)
    (khaoos-run-evil-command-respect-input-method 'avy-goto-word-or-subword-1))

  (evil-define-avy-motion khaoos-avy-goto-word-or-subword-1 exclusive)
)
#+END_SRC
** Persp-mode
#+BEGIN_SRC emacs-lisp
(after! persp-mode
  (setq persp-emacsclient-init-frame-behaviour-override "main"))
#+END_SRC
** Company
#+BEGIN_SRC emacs-lisp
(use-package! company-tabnine
  :when (featurep! :completion company)
  :config
  (setq company-tabnine--disable-next-transform nil)
  (defun my-company--transform-candidates (func &rest args)
    (if (not company-tabnine--disable-next-transform)
        (apply func args)
      (setq company-tabnine--disable-next-transform nil)
      (car args)))

  (defun my-company-tabnine (func &rest args)
    (when (eq (car args) 'candidates)
      (setq company-tabnine--disable-next-transform t))
    (apply func args))

  (advice-add #'company--transform-candidates :around #'my-company--transform-candidates)
  (advice-add #'company-tabnine :around #'my-company-tabnine)
  ;; Trigger completion immediately.
  (setq company-idle-delay 0
        company-minimum-prefix-length 1)

  ;; Number the candidates (use M-1, M-2 etc to select completions).
  (setq company-show-numbers t)

  ;; Use the tab-and-go frontend.
  ;; Allows TAB to select and complete at the same time.
  (company-tng-configure-default)
  (setq company-frontends
        '(company-tng-frontend
          company-pseudo-tooltip-frontend
          company-echo-metadata-frontend)))

(set-company-backend! '(nix-mode)
  '(:separate company-nixos-options
              company-tabnine
              company-files
              company-yasnippet))

;; (set-company-backend! '(org-mode)
;;   '(:separate company-tabnine
;;               company-files
;;               company-yasnippet))

(set-company-backend! '(c-mode
                        c++-mode
                        ;; haskell-mode
                        ;; lisp-mode
                        rust-mode
                        js-mode
                        css-mode
                        web-mode)
  '(:separate company-tabnine
              company-files
              company-yasnippet))

(set-company-backend! '(python-mode) nil)
(set-company-backend! '(python-mode) '(elpy-company-backend :with company-tabnine))

(setq +lsp-company-backends '(company-capf))

(advice-add '+lsp-init-company-backends-h
            :after
            (lambda ()
              (when (eq major-mode 'python-mode)
                (set 'company-backends
                     '(elpy-company-backend
                       company-capf
                       company-yasnippet)))))

(define-key company-active-map (kbd "C-o") 'company-other-backend)

;; Extra bindings for org
;; lsp-org should be used after lsp is loaded in some other mode
(define-key org-mode-map (kbd "C-c l") 'lsp-org)
(define-key org-mode-map (kbd "C-c d") 'lsp-virtual-buffer-disconnect)
#+END_SRC
** Polymode
   indentation in org-mode breaks syntax highlighting
#+BEGIN_SRC emacs-lisp
;; (setq org-startup-indented nil)
;; (require 'polymode)
;; (require 'poly-markdown)
;; (require 'poly-org)

;; (add-hook 'markdown-mode-hook 'poly-markdown-mode)
#+END_SRC
** Tabs
#+BEGIN_SRC emacs-lisp
(global-set-key (kbd "C-x t o") nil)
(global-set-key (kbd "C-x t n") #'tab-next)
(global-set-key (kbd "C-x t p") #'tab-previous)
(global-set-key (kbd "C-x t B") #'tab-bar-mode)
#+END_SRC
** EXWM
   #+BEGIN_SRC emacs-lisp
(let ((exwm-conf "~/.doom.d/exwm-configuration.el"))
  (when (file-exists-p exwm-conf)
    (load-file exwm-conf)))
   #+END_SRC
** Pulseaudio Control
#+BEGIN_SRC emacs-lisp
(setq pulseaudio-control-volume-step "5%")
#+END_SRC
** Subed
#+BEGIN_SRC emacs-lisp
(use-package subed
  ;; Tell emacs where to find subed
  :load-path "~/gits/subed/subed"
  :config
  ;; Disable automatic movement of point by default
  (add-hook 'subed-mode-hook 'subed-disable-sync-point-to-player)
  ;; Remember cursor position between sessions
  (add-hook 'subed-mode-hook 'save-place-local-mode)
  ;; Break lines automatically while typing
  (add-hook 'subed-mode-hook 'turn-on-auto-fill)
   ;; Break lines at 40 characters
  (add-hook 'subed-mode-hook (lambda () (setq-local fill-column 38))))

#+END_SRC
